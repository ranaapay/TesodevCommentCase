﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Configuration
{
    public class ReviewConfiguration : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            builder.HasData
            (
            new Review
            {
                Id = new Guid("5d490a70-94ce-4d15-9494-5248280c2ce3"),
                Content = "deneme yorumu",
                Title = "denemebir",
                Star = 3,
                Status = "approved",
                RejectReason = null,
                OperatedBy = "rana",
                UserId = new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3")     
            },
            new Review
            {
                Id = new Guid("6d490a70-94ce-4d15-9494-5248280c2ce3"),
                Content = "bu da bir deneme yorumudur",
                Title = "denemeiki",
                Star = 4,
                Status = "approved",
                RejectReason = null,
                OperatedBy = "tesodev",
                UserId = new Guid("4d490a70-94ce-4d15-9494-5248280c2ce3")
            },
            new Review
            {
                Id = new Guid("7d490a70-94ce-4d15-9494-5248280c2ce3"),
                Content = "ucuncu deneme yorumu",
                Title = "denemeuc",
                Star = 2,
                Status = "pending",
                RejectReason = null,
                OperatedBy = null,
                UserId = new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3")
            }
            );
        }
    }
}
