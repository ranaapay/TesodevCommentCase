﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Configuration
{

    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasData
            (
            new User
            {
                Id = new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870"),
                Type = "admin",
                Username = "rana",
                Email = "abcd",
                Password = "1234"
            },
            new User
            {
                Id = new Guid("c9d4c053-49b6-410c-bc78-2d54a9991871"),
                Type = "admin",
                Username = "tesodev",
                Email = "efgh",
                Password = "1234"
            },
            new User
            {
                Id = new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3"),
                Type = "user",
                Username = "birinci_kullanici",
                Email = "ijkl",
                Password = "1234"
            },
              new User
              {
                  Id = new Guid("4d490a70-94ce-4d15-9494-5248280c2ce3"),
                  Type = "user",
                  Username = "ikinci_kullanici",
                  Email = "klmn",
                  Password = "1234"
              }
            );
        }
    }
}
