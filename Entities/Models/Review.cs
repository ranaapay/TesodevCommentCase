﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    public class Review
    {
        [Column("ReviewId")]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Content is a required field.")]
        public string Content { get; set; }
        [Required(ErrorMessage = "Title is a required field.")]
        public string Title { get; set; }
        [Range(1, 5,ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [Required(ErrorMessage = "Star is a required field.")]
        public int Star { get; set; }
        [Required(ErrorMessage = "Status is a required field.")]
        public string Status { get; set; }
        public string RejectReason { get; set; }
        public string OperatedBy { get; set; }
        [ForeignKey(nameof(User))]
        public Guid UserId { get; set; }
        public User User { get; set; }

    }
}
