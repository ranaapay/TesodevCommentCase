﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    public class User
    {
        [Column("UserId")]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Type is a required field.")]
        public string Type { get; set; }
        [Required(ErrorMessage = "Username is a required field.")]
        [MaxLength(60, ErrorMessage = "Maximum length for the Username is 60 characters.")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Email address is a required field.")]
        [MaxLength(60, ErrorMessage = "Maximum length for the Address is 60 characters")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Password is a required field.")]
        public string Password { get; set; }
        public ICollection<Review> Reviews { get; set; }
    }

}
