using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace TesodevCommentCase.Controllers
{
    [Route("api/reviews")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private readonly IRepositoryManager _repository;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;

        public ReviewsController(IRepositoryManager repository, ILoggerManager logger, IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult GetApprovedReviews()
        {
             var reviews = _repository.Review.GetAllApprovedReviews(trackChanges: false);
             var reviewsDto = _mapper.Map<IEnumerable<ReviewDto>>(reviews);
             return Ok(reviewsDto);
          
        }
    }
}
