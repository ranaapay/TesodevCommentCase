using AutoMapper;
using Entities.DataTransferObjects;
using Entities.Models;

namespace TesodevCommentCase
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Review, ReviewDto>();
        }
    }

}
