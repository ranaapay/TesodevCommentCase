﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TesodevCommentCase.Migrations
{
    public partial class InitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Email", "Password", "Type", "Username" },
                values: new object[,]
                {
                    { new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870"), "abcd", "1234", "admin", "rana" },
                    { new Guid("c9d4c053-49b6-410c-bc78-2d54a9991871"), "efgh", "1234", "admin", "tesodev" },
                    { new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3"), "ijkl", "1234", "user", "birinci_kullanici" },
                    { new Guid("4d490a70-94ce-4d15-9494-5248280c2ce3"), "klmn", "1234", "user", "ikinci_kullanici" }
                });

            migrationBuilder.InsertData(
                table: "Reviews",
                columns: new[] { "ReviewId", "Content", "OperatedBy", "RejectReason", "Star", "Status", "Title", "UserId" },
                values: new object[] { new Guid("5d490a70-94ce-4d15-9494-5248280c2ce3"), "deneme yorumu", "rana", null, 3, "approved", "denemebir", new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3") });

            migrationBuilder.InsertData(
                table: "Reviews",
                columns: new[] { "ReviewId", "Content", "OperatedBy", "RejectReason", "Star", "Status", "Title", "UserId" },
                values: new object[] { new Guid("7d490a70-94ce-4d15-9494-5248280c2ce3"), "ucuncu deneme yorumu", null, null, 2, "pending", "denemeuc", new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3") });

            migrationBuilder.InsertData(
                table: "Reviews",
                columns: new[] { "ReviewId", "Content", "OperatedBy", "RejectReason", "Star", "Status", "Title", "UserId" },
                values: new object[] { new Guid("6d490a70-94ce-4d15-9494-5248280c2ce3"), "bu da bir deneme yorumudur", "tesodev", null, 4, "approved", "denemeiki", new Guid("4d490a70-94ce-4d15-9494-5248280c2ce3") });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "ReviewId",
                keyValue: new Guid("5d490a70-94ce-4d15-9494-5248280c2ce3"));

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "ReviewId",
                keyValue: new Guid("6d490a70-94ce-4d15-9494-5248280c2ce3"));

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "ReviewId",
                keyValue: new Guid("7d490a70-94ce-4d15-9494-5248280c2ce3"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("c9d4c053-49b6-410c-bc78-2d54a9991871"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("4d490a70-94ce-4d15-9494-5248280c2ce3"));
        }
    }
}
